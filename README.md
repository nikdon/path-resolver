# Path Resolver

An algorithm that identifies dynamic parts and resolves the masked URLs.

It constructs a directed acyclic graph from the parts of the URLs provided as input.


## Quick start

1. Build a java app from the project folder:

    ```bash
    > sbt stage
    ...
    [info] Done packaging.
    [success] Total time: 5 s, completed Jul 30, 2017 8:05:20 PM
    ```

2. Run an app with urls as args:

    ```bash
    > target/universal/stage/bin/path_resolver "/users/Ben/info/location" "/users/Lewis/info/birthday"
    
    Path nodes graph:
    0 - Not dynamic - Node(users)
    1 - Dynamic - Node(Ben), Node(Lewis)
    2 - Not dynamic - Node(info)
    3 - Dynamic - Node(location), Node(birthday)
    
    Masked path: /users/*users_query*/info/*info_query*
    ```