package com.github.nikdon

import com.github.nikdon.domain.Graph

object PathResolver {
  def main(args: Array[String]): Unit = {
    val res = args.toVector.par
      .map(_.split("/").filter(_.nonEmpty).toList)
      .aggregate(Graph.empty)(
        (g, tokens) => Graph.add(tokens, g),
        (g1, g2) => Graph.merge(g1, g2)
      )

    println(s"Path nodes graph: $res")
    println()
    println(s"Masked path: ${res.maskedPath}")
  }
}
