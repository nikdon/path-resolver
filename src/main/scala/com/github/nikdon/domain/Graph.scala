package com.github.nikdon.domain

final case class Graph(layers: List[Layer]) {
  def add(layer: Layer) = Graph(layers :+ layer)

  override def toString: String = {
    layers.zipWithIndex
      .foldLeft(StringBuilder.newBuilder) {
        case (acc, (layer, depth)) =>
          val isDynamicRepr = if (layer.isDynamic) "Dynamic" else "Not dynamic"
          layer.mask match {
            case Some(m) => acc ++= s"\n$depth - $isDynamicRepr / *$m* - $layer"
            case None    => acc ++= s"\n$depth - $isDynamicRepr - $layer"
          }
      }
      .mkString
  }

  def maskedPath: String = {
    val res = Graph.mask(this).layers.foldLeft(StringBuilder.newBuilder) {
      case (acc, layer @ Layer(_, Some(mask))) if layer.isDynamic =>
        acc ++= s"/*$mask*"
      case (acc, layer) =>
        acc ++= s"/${layer.nodes.map(_.value).mkString}"
    }

    res.mkString
  }
}

object Graph {
  def empty = Graph(List.empty)

  def merge(g1: Graph, g2: Graph): Graph = {
    g1.layers.map(Some(_)).zipAll(g2.layers.map(Some(_)), None, None).foldLeft(empty) {
      case (g, (Some(l1), Some(l2))) => g.add(l1 merge l2)
      case (g, (None, Some(l2)))     => g.add(l2)
      case (g, (Some(l1), None))     => g.add(l1)
    }
  }

  def add(tokens: List[String], graph: Graph = empty): Graph = {
    tokens.map(t => Some(Node(t))).zipAll(graph.layers.map(Some(_)), None, None).foldLeft(empty) {
      case (g, (Some(node), Some(layer))) => g.add(layer.add(node))
      case (g, (Some(node), None))        => g.add(Layer(Set(node)))
      case (g, (None, Some(layer)))       => g.add(layer)
    }
  }

  def mask(graph: Graph): Graph = {
    def go(layers: List[Layer], acc: Graph): Graph = layers match {
      case Nil      => acc
      case h :: Nil => acc.add(h)
      case h :: (th :: tt) if th.isDynamic =>
        val prefix = h.nodes
          .map(_.value)
          .reduce((a, b) => (a, b).zipped.takeWhile(Function.tupled(_ == _)).map(_._1).mkString)
        go(tt, acc add h add th.copy(mask = Some(s"${prefix}_query")))
      case h :: t => go(t, acc add h)
    }

    go(graph.layers, empty)
  }
}
