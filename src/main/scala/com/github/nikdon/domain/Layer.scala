package com.github.nikdon.domain

final case class Layer(nodes: Set[Node], mask: Option[String] = None) {
  val size: Int          = nodes.size
  val isDynamic: Boolean = size > 1

  def add(node: Node): Layer = {
    if (nodes.contains(node)) this
    else Layer(nodes + node)
  }

  def merge(that: Layer): Layer = {
    Layer(nodes ++ that.nodes)
  }

  override def toString: String = s"${nodes.mkString(", ")}"
}
